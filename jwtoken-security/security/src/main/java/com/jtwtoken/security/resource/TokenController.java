package com.jtwtoken.security.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jtwtoken.security.entity.JwtUser;
import com.jtwtoken.security.security.JwtGenerator;

@RestController
@RequestMapping("/token")
public class TokenController {

	private JwtGenerator generator;

	public TokenController(JwtGenerator generator) {
		this.generator = generator;
	}

	/*
	 * @PostMapping public String getToken(@RequestBody final JwtUser user) { return
	 * generator.generate(user); }
	 */

	@PostMapping
	public ResponseEntity<List<String>> getToken(@RequestBody String userName) {
		JwtUser user = new JwtUser();
		user.setUserName(userName);
		user.setId(10);
		user.setRole("user");
		List<String> result = new ArrayList<String>();
		result.add(generator.generate(user));
		return ResponseEntity.ok(result);
	}
}
