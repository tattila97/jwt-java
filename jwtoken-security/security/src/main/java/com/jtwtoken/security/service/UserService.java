package com.jtwtoken.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jtwtoken.security.entity.User;
import com.jtwtoken.security.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User loeadUserByUsername(String userName) {
		return userRepository.findByUsername(userName);
	}

	public User save(User user) {
		return userRepository.save(user);
	}

}
