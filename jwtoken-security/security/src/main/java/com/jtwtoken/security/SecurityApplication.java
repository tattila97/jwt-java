package com.jtwtoken.security;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.jtwtoken.security.entity.Role;
import com.jtwtoken.security.entity.User;
import com.jtwtoken.security.service.UserService;

@SpringBootApplication
public class SecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityApplication.class, args);
	}

	@Bean
	public CommandLineRunner setupDefaultUser(UserService service) {
		System.out.println("Add user ");
		return args -> {
			service.save(new User("user", "user", Stream.of(new Role("USER")).collect(Collectors.toSet())));
		};
	}

}
