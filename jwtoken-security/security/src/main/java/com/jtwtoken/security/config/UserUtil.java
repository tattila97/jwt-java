package com.jtwtoken.security.config;

import org.springframework.security.core.context.SecurityContextHolder;

import com.jtwtoken.security.entity.JwtUserDetails;

public class UserUtil {

	public static JwtUserDetails getAuthenticatedUser() {
		Object authentication = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if ((authentication instanceof JwtUserDetails)) {
			return (JwtUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}
		return null;
	}

}
