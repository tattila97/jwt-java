package com.jtwtoken.security.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jtwtoken.security.config.UserUtil;
import com.jtwtoken.security.entity.JwtUserDetails;

@RestController
@RequestMapping()
public class HelloController {

	@GetMapping("/hello")
	public String hello() {
		return "Hello boys";
	}

	@GetMapping("/api/hello")
	public String authHello() {
		return "Hello My Lord";

	}

	@GetMapping("/api/me")
	public String getMyUserName() {
		return UserUtil.getAuthenticatedUser().getUsername();
	}

	@GetMapping("/api/me/object")
	public JwtUserDetails getMyUser() {
		return UserUtil.getAuthenticatedUser();
	}

}
